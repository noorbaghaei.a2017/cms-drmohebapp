<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('permissions')->delete();

//        $permissions=[
//            'article-full-access',
//            'article-create',
//            'article-edit',
//            'article-delete',
//            'role-full-access',
//            'role-create',
//            'role-edit',
//            'role-delete',
//            'user-full-access',
//            'user-create',
//            'user-edit',
//            'user-delete',
//            'information-full-access',
//            'information-create',
//            'information-edit',
//            'information-delete'
//        ];
//
//        foreach ($permissions as $permission) {
//            Permission::create(['name' => $permission]);
//        }


    }
}
