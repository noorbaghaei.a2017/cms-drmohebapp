@extends('template.app')

@section('content')

    <!-- cart-main-area start -->
    <div class="cart-main-area section-padding--lg bg--white" style="direction: rtl">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 ol-lg-4">
<ul class="text-right">
    <li><a>پروفایل</a></li>
    <li><a>کلاس ها</a></li>
    <li> <a>مسابقات</a></li>
</ul>
                </div>
                <div class="col-md-8 col-sm-12 ol-lg-8">
                    <table >
                        <thead>
                        <tr >
                            <th class="text-right">ردیف</th>
                            <th class="text-right">نام کلاس</th>
                            <th class="text-right">تاریخ پایان کلاس</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr >
                            <td class="text-right">1</td>
                            <td class="text-right">رباتیک</td>
                            <td class="text-right">سه شنبه, 11 شهریور 99</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
{{--            <div class="row">--}}
{{--                <div class="col-lg-6 offset-lg-6">--}}
{{--                    <div class="cartbox__total__area">--}}
{{--                        <div class="cartbox-total d-flex justify-content-between">--}}
{{--                            <ul class="cart__total__list">--}}
{{--                                <li>Cart total</li>--}}
{{--                                <li>Sub Total</li>--}}
{{--                            </ul>--}}
{{--                            <ul class="cart__total__tk">--}}
{{--                                <li>$70</li>--}}
{{--                                <li>$70</li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                        <div class="cart__total__amount">--}}
{{--                            <span>Grand Total</span>--}}
{{--                            <span>$140</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
    <!-- cart-main-area end -->


@endsection


