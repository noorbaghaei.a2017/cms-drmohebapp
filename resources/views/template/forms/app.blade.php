@include('template.forms.sections.header')

<div class="wrapper full-screen-form">
@yield('content')
</div>

@include('template.forms.sections.footer')
