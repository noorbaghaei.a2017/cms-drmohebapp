@extends('template.forms.app')

@section('content')

    @if ($errors->any())
        <form action="#" id="wizard" method="POST">
            <!-- SECTION 1 -->
            <h4></h4>
            <section>
                <div class="inner">

                    <div class="form-content">
                        <div class="form-inner form-inner-last">
                            <div class="ready">
                                <a href="{{route('front.website')}}">
                                    @if($setting->Hasmedia('logo'))
                                        <img src="{{$setting->getFirstMediaUrl('logo')}}" width="80px" style="padding-bottom: 5px">
                                    @else
                                        <img src="{{asset('img/no-img.gif')}}" width="80px" style="padding-bottom: 5px">
                                    @endif
                                </a>
                                <span>
										<i class="zmdi zmdi-close"></i>
									</span>
                                <div class="alert " style="color: #a94442;">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </form>

    @else
        <form action="{{route('send.request.female')}}" id="wizard" method="POST" enctype="multipart/form-data">
            @csrf
            <!-- SECTION 1 -->
            <h4></h4>
            <section>
                <div class="inner">

                    <div class="form-content">
                        <div class="form-inner">
                            <div class="form-header">

                                <a  class="btn-home-page" href="{{route('front.website')}}" style="margin-bottom: 55px;margin-top: 10px">Home Page</a>
                                <h3> Welcher Bereich stört Sie?</h3>
                                <p>~ Welcher Bereich stört Sie? ~</p>
                            </div>

                            <div class="form-row">
                                <div class="row text-center parent-ihr">
                                    <div class="col-lg-2 col-lg-push-2 col-md-12 ">

                                        <input class="ihr" id="Geheimratsecken" type="radio" name="ihr" value="Geheimratsecken"  checked>
                                        <label class="replace-radio active-input" for="Geheimratsecken">
                                            <img src="{{asset('template/images/female/W4.png')}}" >

                                        </label>
                                        <span>Geheimratsecken</span>

                                    </div>
                                    <div class="col-lg-2 col-lg-push-2 col-md-12">

                                        <input class="ihr" id="Tonsur" type="radio" name="ihr" value="Tonsur" >

                                        <label  class="replace-radio" for="Tonsur">
                                            <img src="{{asset('template/images/female/W1.png')}}" >
                                        </label>
                                        <span>Tonsur</span>
                                    </div>
                                    <div class="col-lg-2 col-lg-push-2 col-md-12">

                                        <input class="ihr" id="Große Tonsur" type="radio" name="ihr" value="Große Tonsur" >

                                        <label class="replace-radio" for="Große Tonsur">
                                            <img src="{{asset('template/images/female/W3.png')}}" >
                                        </label>
                                        <span>Große Tonsur</span>
                                    </div>
                                    <div class="col-lg-2 col-lg-push-2 col-md-12">

                                        <input class="ihr" id="Leichte Glatze" type="radio" name="ihr" value="Leichte Glatze" >

                                        <label class="replace-radio" for="Leichte Glatze">
                                            <img src="{{asset('template/images/female/W2.png')}}" >
                                        </label>
                                        <span>Leichte Glatze</span>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- SECTION 2 -->
            <h4></h4>
            <section>
                <div class="inner">

                    <div class="form-content">
                        <div class="form-inner">
                            <div class="form-header">

                                <a  class="btn-home-page" href="{{route('front.website')}}" style="margin-bottom: 55px;margin-top: 10px">Home Page</a>

                                <h3>
                                    Welche Haarfarbe / haben Sie?
                                </h3>
                                <p>~ Welche Haarfarbe haben Sie? ~</p>
                            </div>
                            <div class="form-row">
                                <div class="row text-center parent-blond">
                                    <div class="col-lg-2 col-lg-push-1 col-md-12">

                                        <input id="Blond" type="radio" name="blond" value="Blond" checked>

                                        <label class="replace-radio active-input" for="Blond">
                                            <img src="{{asset('template/images/female/color/blond-w.png')}}">
                                        </label>
                                        <span>Blond</span>

                                    </div>
                                    <div class="col-lg-2 col-lg-push-1 col-md-12">

                                        <input id="Braun" type="radio" name="blond" value="Braun" >

                                        <label class="replace-radio" for="Braun">
                                            <img src="{{asset('template/images/female/color/braun-w.png')}}">
                                        </label>
                                        <span>Braun</span>
                                    </div>
                                    <div class="col-lg-2 col-lg-push-1 col-md-12">

                                        <input id="grau" type="radio" name="blond" value="grau" >

                                        <label class="replace-radio" for="grau">
                                            <img src="{{asset('template/images/female/color/grau-w.png')}}">
                                        </label>
                                        <span>grau</span>
                                    </div>
                                    <div class="col-lg-2 col-lg-push-1 col-md-12">

                                        <input id="Rot" type="radio" name="blond" value="Rot" >

                                        <label class="replace-radio" for="Rot">
                                            <img src="{{asset('template/images/female/color/rot-w.png')}}">
                                        </label>
                                        <span>Rot</span>
                                    </div>

                                    <div class="col-lg-2 col-lg-push-1 col-md-12">

                                        <input id="Schwarz" type="radio" name="blond" value="Schwarz" >

                                        <label class="replace-radio" for="Schwarz">
                                            <img src="{{asset('template/images/female/color/schwarz-w.png')}}">
                                        </label>
                                        <span>Schwarz</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <!-- SECTION 3 -->
            <h4></h4>
            <section>
                <div class="inner">

                    <div class="form-content">
                        <div class="form-inner">
                            <div class="form-header">
                                <a  class="btn-home-page" href="{{route('front.website')}}" style="margin-bottom: 55px;margin-top: 10px">Home Page</a>

                                <h3>
                                    Seit wann leiden Sie unter Haarausfall?

                                </h3>
                                <p>~ Seit wann leiden Sie unter Haarausfall? ~</p>
                            </div>

                            <div class="form-row">
                                <div class="row text-center">
                                    <div class="col-lg-8 col-lg-push-2">
                                        <label style="font-size: 18px">
                                            < 1 Jahr   / > 10 Jahre
                                        </label>
                                        <input id="Jahr" type="range" name="Jahr" min="1" max="10" value="1"  onchange="updateTextInput(this.value);">
                                        <br>
                                        <img src="{{asset('template/images/informieren.svg')}}" >
                                        <label for="Geheimratsecken klein" style="font-size: 18px">
                                            Jahr Value :
                                        </label>
                                        <label id="textInput" style="font-size: 26px">
                                            1
                                        </label>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <!-- SECTION 4 -->
            <h4></h4>
            <section>
                <div class="inner">

                    <div class="form-content">
                        <div class="form-inner">
                            <div class="form-header">

                                <a  class="btn-home-page" href="{{route('front.website')}}" style="margin-bottom: 55px;margin-top: 10px">Home Page</a>

                                <h3>
                                    Hatten Sie bereits eine Haartransplantation?
                                </h3>
                                <p>~ Hatten Sie bereits eine Haartransplantation?  ~</p>
                            </div>

                            <div class="form-row">
                                <div class="row text-center parent-eine">

                                    <div class="col-lg-2 col-lg-push-4">

                                        <input id="Ja" type="radio" name="eine" value="Ja" checked>

                                        <label class="replace-radio active-input" for="Ja">
                                            <img src="{{asset('template/images/female/ja-w.png')}}">
                                        </label>
                                        <span>Ja</span>

                                    </div>
                                    <div class="col-lg-2 col-lg-push-4">

                                        <input id="Nein" type="radio" name="eine" value="Nein" >

                                        <label class="replace-radio" for="Nein">
                                            <img src="{{asset('template/images/female/nein-w.png')}}">
                                        </label>
                                        <span>Nein</span>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <!-- SECTION 5 -->
            <h4></h4>
            <section>
                <div class="inner">

                    <div class="form-content">
                        <div class="form-inner">
                            <div class="form-header">

                                <a  class="btn-home-page" href="{{route('front.website')}}" style="margin-bottom: 55px;margin-top: 10px">Home Page</a>

                                <h3>Wie schlimm empfinden Sie Ihre aktuelle Haarsituation?</h3>
                                <p>~ Wie schlimm empfinden Sie Ihre aktuelle Haarsituation? ~</p>
                            </div>

                            <div class="form-row">
                                <div class="col-lg-6 col-md-6 col-sm-2 col-xs-6 text-right">

                                    <img src="{{asset('template/images/termometer2.png')}}">


                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-9 col-xs-6" style="margin-top: 24px">
                                    <div class="row text-left parent-schlimm">
                                        <div class="col-lg-12" style="padding-bottom: 25px">

                                            <input id="Sehr schlimm" type="radio" name="schlimm" value="Sehr schlimm" checked>
                                            <label class="replace-text active-input" for="Sehr schlimm">
                                                Sehr schlimm
                                            </label>

                                        </div>
                                        <div class="col-lg-12 "  style="padding-bottom: 25px">

                                            <input id="schlimm" type="radio" name="schlimm" value="schlimm" >
                                            <label class="replace-text" for="schlimm">
                                                schlimm
                                            </label>

                                        </div>
                                        <div class="col-lg-12"  style="padding-bottom: 25px">

                                            <input id="etwas schlimm" type="radio" name="schlimm" value="etwas schlimm" >
                                            <label class="replace-text" for="etwas schlimm">
                                                etwas schlimm
                                            </label>

                                        </div>
                                        <div class="col-lg-12"  style="padding-bottom: 25px">

                                            <input id=" nicht schlimm" type="radio" name="schlimm" value=" nicht schlimm" >
                                            <label class="replace-text" for=" nicht schlimm">
                                                nicht schlimm
                                            </label>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>



            <!-- SECTION 6 -->
            <h4></h4>
            <section>
                <div class="inner">

                    <div class="form-content">
                        <div class="form-inner">
                            <div class="form-header">
                                <a  class="btn-home-page" href="{{route('front.website')}}" style="margin-bottom: 55px;margin-top: 10px">Home Page</a>

                                <h3>
                                    Wann soll die Behandlung stattfinden?
                                </h3>
                                <p>~  Wann soll die Behandlung stattfinden? ~</p>
                            </div>
                            <div class="form-row">
                                <div class="row text-center parent-behandlung">
                                    <div class="col-lg-2 col-lg-push-2">

                                        <input id="So schnell wie möglich" type="radio" name="behandlung" value="So schnell wie möglich" checked>

                                        <label class="replace-radio active-inputh" for="So schnell wie möglich">
                                            <img src="{{asset('template/images/informieren.svg')}}">
                                        </label>
                                        <span>So schnell wie möglich</span>

                                    </div>
                                    <div class="col-lg-2 col-lg-push-2">

                                        <input id="In den nächsten 3 Monaten" type="radio" name="behandlung" value="In den nächsten 3 Monaten" >

                                        <label class="replace-radio" for="In den nächsten 3 Monaten">
                                            <img src="{{asset('template/images/informieren.svg')}}">
                                        </label>
                                        <span>In den nächsten 3 Monaten</span>
                                    </div>
                                    <div class="col-lg-2 col-lg-push-2">

                                        <input id=" In den nächsten 12 Monaten" type="radio" name="behandlung" value=" In den nächsten 12 Monaten" >

                                        <label class="replace-radio" for=" In den nächsten 12 Monaten">
                                            <img src="{{asset('template/images/informieren.svg')}}">
                                        </label>
                                        <span>In den nächsten 12 Monaten</span>
                                    </div>
                                    <div class="col-lg-2 col-lg-push-2">

                                        <input id="Ich möchte mich nur informieren" type="radio" name="behandlung" value="Ich möchte mich nur informieren" >

                                        <label class="replace-radio" for="Ich möchte mich nur informieren">
                                            <img src="{{asset('template/images/informieren.svg')}}">
                                        </label>
                                        <span>Ich möchte mich nur informieren</span>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

                <!-- SECTION 7 -->
                <h4></h4>
                <section>
                    <div class="inner">

                        <div class="form-content">
                            <div class="form-inner">
                                <div class="form-header">

                                    <a  class="btn-home-page" href="{{route('front.website')}}" style="margin-bottom: 55px;margin-top: 10px">Home Page</a>

                                    <h3>Fast Geschafft...</h3>
                                    <p>~ Wer soll die kostenlose Haaranalyse erhalten? ~</p>
                                </div>
                                <div class="form-row">
                                    <div class="row">
                                        <div class="col-lg-6 col-lg-push-3">
                                            <label for="">
                                                <i class="zmdi zmdi-account-o"></i>
                                                Name
                                            </label>
                                            <input type="text" name="name" class="form-control" placeholder="Vor- und Nachname*" required>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-lg-push-3">
                                            <label for="">
                                                <i class="zmdi zmdi-email"></i>
                                                email
                                            </label>
                                            <input type="email" name="email" class="form-control" placeholder="Ihre Email-Adresse*" required>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-lg-push-3">
                                            <label for="">
                                                <i class="zmdi zmdi-phone"></i>
                                                phone
                                            </label>
                                            <input name="phone" type="text" class="form-control" placeholder="Handynummer*" required>

                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px">
                                        <div class="col-lg-2 col-lg-push-3">
                                            <label for="">
                                                <i class="zmdi zmdi-file"></i>
                                                Foto von Vorne
                                            </label>
                                            <input type="file" name="Vorne" class="form-control" >
                                            <img src="{{asset('template/images/F1.png')}}" style="padding: 10px">

                                        </div>
                                        <div class="col-lg-2 col-lg-push-3">
                                            <label for="">
                                                <i class="zmdi zmdi-file"></i>
                                                Foto von Hinten
                                            </label>
                                            <input type="file" name="Hinten" class="form-control" >
                                            <img src="{{asset('template/images/F2.png')}}" style="padding: 10px">

                                        </div>
                                        <div class="col-lg-2 col-lg-push-3">
                                            <label for="">
                                                <i class="zmdi zmdi-file"></i>
                                                Foto von Oben
                                            </label>
                                            <input type="file" name="Oben" class="form-control" >
                                            <img src="{{asset('template/images/F3.png')}}" style="padding: 10px">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div>
                                            <input type="submit" id="submit-form" value="Send">
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </section>
            <!-- SECTION 8 -->
            <h4></h4>
            <section>
                <div class="inner">

                    <div class="form-content">
                        <div class="form-inner form-inner-last">
                            <div class="ready">
									<span>
										<i class="zmdi zmdi-check"></i>
									</span>
                                <p class="text-1">Tank dich</p>
                                <p class="text-2">~ Schönen Tag ~</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    @endif
@endsection


@if($errors->any())

@section('head')


    <style>
        .steps.clearfix{
            display: none;
        }
    </style>

@endsection

@endif

@section('head')

    <style>
        [type=radio] {
            position: absolute;
            opacity: 0;
            width: 0;
            height: 0;
        }
        .replace-radio{
            padding: 35px;
            position: relative;
            box-shadow: 0 -2px 10px #00000017;
            border-radius: 15px;
            -webkit-transition: all 200ms ease;
            transition: all 200ms ease;
            cursor: pointer;
        }
        .replace-text{
            position: relative;
            padding: 35px;
            box-shadow: 0 -2px 10px #00000017;
            border-radius: 15px;
            -webkit-transition: all 200ms ease;
            transition: all 200ms ease;
            cursor: pointer;
        }
        .replace-text:hover{
            border-style: solid;
            border-width: 4px;
            border-color: #dd7359;
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
        }
        .replace-radio:hover{
            border-style: solid;
            border-width: 4px;
            border-color: #dd7359;
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
        }
        /* CHECKED STYLES */
        [type=radio]:checked + img {
            /*outline: 2px solid #f00;*/
        }
        .form-row span,
        .form-row span{
            color: #fdfdfd !important;
        }
    </style>

@endsection

@section('script')

<script>

    $(document).ready(function() {
        $("input[name='ihr']").click(function () {

            $('.parent-ihr label').removeClass("active-input")
           $(this).siblings('label.replace-radio').addClass("active-input");


        });
        $("input[name='blond']").click(function () {

            $('.parent-blond label').removeClass("active-input")
            $(this).siblings('label.replace-radio').addClass("active-input");


        });
        $("input[name='eine']").click(function () {


            $('.parent-eine label').removeClass("active-input")
            $(this).siblings('label.replace-radio').addClass("active-input");


        });
        $("input[name='schlimm']").click(function () {

            $('.parent-schlimm label').removeClass("active-input")
            $(this).siblings('label.replace-text').addClass("active-input");


        });
        $("input[name='behandlung']").click(function () {

            $('.parent-behandlung label').removeClass("active-input")
            $(this).siblings('label.replace-radio').addClass("active-input");


        });
    });
</script>

@endsection




