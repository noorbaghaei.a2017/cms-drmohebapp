
<script src="{{asset('template/js/jquery-3.5.1.min.js')}}"></script>

<!-- DATE-PICKER -->
<script src="{{asset('template/vendor/date-picker/js/datepicker.js')}}"></script>
<script src="{{asset('template/vendor/date-picker/js/datepicker.en.js')}}"></script>

<!-- JQUERY STEP -->
<script src="{{asset('template/js/jquery.steps-wizard.js')}}"></script>

<script src="{{asset('template/js/wizard-7.js')}}"></script>


<script>

    function updateTextInput(val) {
        document.getElementById('textInput').textContent=val;
    }

</script>

@yield('script')

<!-- Template created and distributed by Colorlib -->
</body>
</html>
