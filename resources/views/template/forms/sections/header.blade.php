<!DOCTYPE html>
<html>
<head>
    {!! SEO::generate() !!}

    @yield('seo')

    <meta charset="utf-8" />

    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="colorlib.com">

    <!-- MATERIAL DESIGN ICONIC FONT -->
    <link rel="stylesheet" href="{{asset('template/fonts/material-design-iconic-font/css/material-design-iconic-font.css')}}">

    <!-- DATE-PICKER -->
    <link rel="stylesheet" href="{{asset('template/vendor/date-picker/css/datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/bootstrap.min.css')}}">

    <!-- STYLE CSS -->
    <link rel="stylesheet" href="{{asset('template/css/style.css')}}">

    <style>
        .active-input{
            border-color: #dd7359;
            border-style: solid;
            border-width: 4px;
        }
    </style>


    @yield('head')
</head>
<body>

