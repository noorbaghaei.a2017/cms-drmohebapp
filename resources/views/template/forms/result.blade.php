@extends('template.forms.app')

@section('content')

    <form action="#" id="wizard" method="POST">
    <!-- SECTION 1 -->
        <h4></h4>
        <section>
            <div class="inner">

                <div class="form-content">
                    <div class="form-inner form-inner-last">
                        <div class="ready">
                            <a href="{{route('front.website')}}">
                                @if($setting->Hasmedia('logo'))
                                    <img src="{{$setting->getFirstMediaUrl('logo')}}" width="80px">
                                @else
                                    <img src="{{asset('img/no-img.gif')}}" width="80px">
                                @endif
                            </a>
                           @if(session()->has('message'))
                                <span>
										<i class="zmdi zmdi-check"></i>
									</span>
                                <p class="text-1">Thank You</p>
                                <p class="text-2">~ Schönen Tag ~</p>

                                @elseif(session()->has('error'))
                                    <div class="alert alert-danger" >
                                        {{session()->get('error')}}
                                    </div>

                            @else
                                <p class="text-1">Dont Request</p>
                                <p class="text-2">~ Dont Request ~</p>
                           @endif

                        </div>
                    </div>
                </div>
            </div>
        </section>


    </form>

@endsection

@section('head')

    <style>
        .steps.clearfix{
            display: none;
        }
    </style>
@endsection








