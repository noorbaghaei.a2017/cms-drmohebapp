@extends('template.app')

@section('content')


    <!-- End Bradcaump area -->
    <!-- Start Choose Us Area -->
    <section class="dcare__choose__us__area section-padding--lg bg--white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section__title text-center">
                        <h2 class="title__line">ویژگی ها سیگما ربات</h2>
                    </div>
                </div>
            </div>
            <div class="row mt--40">
                <!-- Start Single Choose Option -->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="dacre__choose__option">
                        <!-- Start Single Choose -->
                        @foreach($properties as $key=>$property)
                        <div class="choose">
                            <div class="choose__inner">
                                <h4><a href="#">{{$property->title}}</a></h4>
                                <p>
                                    {{$property->text}}
                                </p>
                            </div>
                            <div class="choose__icon">
                                <img src="{{asset('template/images/choose/icon/'.($key+1).'.png')}}" alt="choose icon">
                            </div>
                        </div>
                       @endforeach
                        <!-- End Single Choose -->

                    </div>
                </div>
                <!-- End Single Choose Option -->
                <!-- Start Single Choose Option -->
                <div class="col-lg-6 col-md-6 col-sm-12 d-block d-lg-block d-md-none">
                    <div class="dacre__choose__option">
                        <div class="choose__big__img">
                            <img src="{{asset('template/images/choose/big-img/1.png')}}" alt="choose images">
                        </div>
                    </div>
                </div>
                <!-- End Single Choose Option -->

            </div>
        </div>
    </section>
    <!-- End Choose Us Area -->

    <!-- Start Courses Area -->
    <section class="dcare__courses__area section-padding--lg bg--white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section__title text-center">
                        <h2 class="title__line">کلاس های ما</h2>
                    </div>
                </div>
            </div>
            <div class="row mt--40" style="direction: rtl">
                <!-- Start Single Courses -->
                @foreach($courses as $course)
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="courses">
                        <div class="courses__thumb">
                            <a href="{{route('courses.single',['course'=>$course->slug])}}">

                                @if(!$course->Hasmedia('images'))
                                    <img src="{{asset('template/images/courses/1.jpg')}}" alt="courses images">
                                @else

                                    <img src="{{$course->getFirstMediaUrl('images')}}" alt="courses images" >
                                @endif
                            </a>
                        </div>
                        <div class="courses__inner text-right">
                            <ul class="courses__meta d-flex">
                                <li class="prize">$50</li>
                                <li class="comment"><i class="fa fa-comment"></i>53</li>
                                <li class="like"><i class="fa fa-user"></i>40</li>
                            </ul>
                            <div class="courses__wrap">
                                <div class="courses__date"><i class="fa fa-calendar"></i>30th Dec, 2017</div>
                                <div class="courses__content">
                                    <h4><a href="{{route('courses.single',['course'=>$course->slug])}}">{{$course->title}}</a></h4>
                                   <p>
                                       {{$course->excerpt}}
                                   </p>
                                    <ul class="rating d-flex">
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
                <!-- End Single Courses -->

            </div>
        </div>
    </section>
    <!-- End Courses Area -->
    <!-- Start Team Area -->
    <section class="dcare__team__area pb--150 bg--white" style="direction: rtl">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section__title text-center">
                        <h2 class="title__line">اساتید ما</h2>
                    </div>
                </div>
            </div>
            <div class="row mt--40">

                <!-- Start Single Team -->
                @foreach($professors as $professor)
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="team">
                        <div class="team__thumb">
                            <a href="#">
                                <img src="{{asset('template/images/amin-nourbaghaei.jpeg')}}" alt="team images">
                            </a>
                            <div class="team__hover__action">
                                <div class="team__hover__inner">
                                    <ul class="dacre__social__link--2 d-flex justify-content-center">
                                        <li class="facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                        <li class="vimeo"><a href="https://vimeo.com/"><i class="fa fa-vimeo"></i></a></li>
                                        <li class="pinterest"><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="team__details">
                            <div class="team__info">
                                <h6><a href="#">{{$professor->full_name}}</a></h6>
                                <span>برنامه نویس</span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
                <!-- End Single Team -->
            </div>
        </div>
    </section>
    <!-- End Team Area -->
    <!-- Start Counter Up Area -->
    <section class="dcare__counterup__area section-padding--lg bg-image--6" style="direction: rtl">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="counterup__wrapper d-flex flex-wrap flex-lg-nowrap flex-md-nowrap justify-content-between">
                        <!-- Start Single Fact -->
                        <div class="funfact">
                            <div class="fact__icon">
                                <img src="{{asset('template/images/funfact/1.png')}}" alt="flat icon">
                            </div>
                            <div class="fact__count ">
                                <span class="count">95</span>
                            </div>
                            <div class="fact__title">
                                <h2>دانش آموزان</h2>
                            </div>
                        </div>
                        <!-- End Single Fact -->
                        <!-- Start Single Fact -->
                        <div class="funfact">
                            <div class="fact__icon">
                                <img src="{{asset('template/images/funfact/2.png')}}" alt="flat icon">
                            </div>
                            <div class="fact__count ">
                                <span class="count color--2">95</span>
                            </div>
                            <div class="fact__title">
                                <h2>کلاس ها</h2>
                            </div>
                        </div>
                        <!-- End Single Fact -->
                        <!-- Start Single Fact -->
                        <div class="funfact">
                            <div class="fact__icon">
                                <img src="{{asset('template/images/funfact/3.png')}}" alt="flat icon">
                            </div>
                            <div class="fact__count ">
                                <span class="count color--3">40</span>
                            </div>
                            <div class="fact__title">
                                <h2>اساتید</h2>
                            </div>
                        </div>
                        <!-- End Single Fact -->
                        <!-- Start Single Fact -->
                        <div class="funfact">
                            <div class="fact__icon">
                                <img src="{{asset('template/images/funfact/4.png')}}" alt="flat icon">
                            </div>
                            <div class="fact__count">
                                <span class="count color--4">10</span>
                            </div>
                            <div class="fact__title">
                                <h2>مسابقات</h2>
                            </div>
                        </div>
                        <!-- End Single Fact -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Counter Up Area -->



@endsection
