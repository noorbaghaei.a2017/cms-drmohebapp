﻿@extends('template.app')

@section('content')

    <div class="wrapper wrapper--w690" >
        <div class="card card-1">

            <div class="card-body" style="height: 700px">

<div class="card-header text-center" style="padding: 30px">
    <h2 style="color:#fdfdfd;padding: 20px;display: inline-block;border-bottom: 2px solid #fdfdfd;">
        Bitte wählen Sie Ihr Geschlecht
    </h2>
</div>

                        <div class="tab-content">
                            <div class="row">
                                <div class="tab-pane active" id="tab1" style="margin:0 auto;">
                                <div class="col-lg-6">

                                    <div class="input-group hover-image">
                                        <label class="label">Weiblich</label>
                                        <a href="{{route('female.hair')}}" >
                                            <img src="{{asset('template/images/male.png')}}">
                                        </a>
                                    </div>

                                </div>
                                    <div class="col-lg-6">

                                        <div class="input-group hover-image">
                                            <label class="label">Männlich</label>
                                            <a href="{{route('male.hair')}}" class="hover-image">
                                                <img src="{{asset('template/images/female.png')}}">
                                            </a>
                                        </div>
                                    </div>

                                </div>



                            </div>

                        </div>

            </div>
        </div>
    </div>

   @include('template.sections.articles')



@endsection

@section('head')

<style>

    [type=radio] {
        position: absolute;
        opacity: 0;
        width: 0;
        height: 0;
    }
    .replace-radio{
        padding: 35px;
        position: relative;
        box-shadow: 0 -2px 10px #00000017;
        border-radius: 15px;
        -webkit-transition: all 200ms ease;
        transition: all 200ms ease;
        cursor: pointer;
    }
    .replace-radio:hover{
        border-style: solid;
        border-width: 4px;
        border-color: #dd7359;
        -webkit-transform: scale(1.1);
        -ms-transform: scale(1.1);
        transform: scale(1.1);
    }
    .replace-text{
        position: relative;
        padding: 35px;
        box-shadow: 0 -2px 10px #00000017;
        border-radius: 15px;
        -webkit-transition: all 200ms ease;
        transition: all 200ms ease;
        cursor: pointer;
    }
    .replace-text span {
        font-size: 16px;
        padding: 50px;


    }
    .replace-radio span {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        text-align: center;

    }
    .replace-text:hover{
        border-style: solid;
        border-width: 4px;
        border-color: #dd7359;
        -webkit-transform: scale(1.1);
        -ms-transform: scale(1.1);
        transform: scale(1.1);
    }

    /* IMAGE STYLES */
    [type=radio] + img {
        cursor: pointer;
    }
    [type=submit]  {
        margin: 10px;
    }

    /* CHECKED STYLES */
    [type=radio]:checked + img {
        /*outline: 2px solid #f00;*/
    }
    h5.title-text{
        color: #767d85;
        text-align: center;
        margin: 5px 5px 30px 5px;
        text-transform: uppercase;
        font-size: 24px;
        line-height: 30px;
        font-weight: 400;
    }
    .information input {
    height: 1.8em !important;
    line-height: 0  !important;

    }
    .input-group.information{
        margin: 5px 0;
    }

</style>
@endsection





