@extends('template.app')

@section('content')



    <div class="wrapper wrapper--w690" >

                <div class="text-center"  style="margin-top: 15px">

                    @if(!$item->Hasmedia('images'))
                        <img src="{{asset('img/no-img.gif')}}" alt="" >
                    @else

                        <img src="{{$item->getFirstMediaUrl('images')}}" alt="" >
                    @endif


                    <h2 style="margin-top: 15px">
                        {{$item->title}}
                    </h2>
                </div>


        <div class="text-center" style="margin-top: 15px">
            {!! $item->text !!}
        </div>


    </div>


@endsection
