<!-- Footer -->
<footer id="footer">
    <div class="inner">
        @if($setting->Hasmedia('logo'))
            <img src="{{$setting->getFirstMediaUrl('logo')}}" style="width: 80px">
        @else
            <img src="{{asset('img/no-img.gif')}}" style="width: 80px">
        @endif
        <h2>{{$setting->excerpt}}</h2>
        <ul class="actions">
            <li><span class="icon fa-phone"></span> <a href="tell:{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}">{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}</a></li>
            <li><span class="icon fa-envelope"></span> <a href="mailto:{{$setting->email}}">{{$setting->email}}</a></li>
            <li><span class="icon fa-map-marker"></span> {{$setting->address}}</li>
        </ul>
    </div>
    <div class="copyright">
        &copy; Untitled. Design <a href="https://moheb.hamburg/">Drmoheb</a>.
    </div>
</footer>



<!-- Scripts -->
<script src="{{asset('template/js/jquery.min.js')}}"></script>
<script src="{{asset('template/js/jquery.scrolly.min.js')}}"></script>
<script src="{{asset('template/js/skel.min.js')}}"></script>
<script src="{{asset('template/js/util.js')}}"></script>
<script src="{{asset('template/js/main.js')}}"></script>



<script src="{{asset('template/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('template/js/bootstrap.min.js')}}"></script>
<script src="{{asset('template/js/jquery.bootstrap.wizard.min.js')}}"></script>

{{--<!-- Main JS-->--}}
<script src="{{asset('template/js/global.js')}}"></script>






@yield('script')

</body>
</html>
