<!DOCTYPE HTML>
<!--
	Intensify by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    {!! SEO::generate() !!}

    @yield('seo')

    <meta charset="utf-8" />

    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="{{asset('template/css/main.css')}}" />
    <link rel="stylesheet" href="{{asset('template/css/bootstrap.min.css')}}" />
        <link href="{{asset('template/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">
        <link href="{{asset('template/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
        <link href="{{asset('template/css/step.css')}}" rel="stylesheet" media="all">
<style>
    .active-input{
        border-color: #dd7359;
        border-style: solid;
        border-width: 4px;
    }
</style>

    @yield('head')
</head>
<body>

<!-- Header -->
<header id="header">
    <nav class="left">
        <a href="#menu"><span>Menu</span></a>
    </nav>
    <a href="{{route('front.website')}}" class="logo">DrMoheb</a>

</header>

<!-- Menu -->
<nav id="menu">
    <ul class="links">
        @foreach($top_menus as $menu)
            <li ><a href="{{$menu->href}}">{{$menu->symbol}}</a></li>
        @endforeach

    </ul>

</nav>
