<?php

namespace Modules\Payment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;

class Pay extends Model
{
    use TimeAttribute;

    protected $fillable = ['token','user','amount','description','authority','card_number','trace_number','status','gateway','type'];


}
