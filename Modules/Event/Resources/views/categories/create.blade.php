@include('core::layout.modules.category.create',[

    'title'=>__('core::categories.create'),
    'parent'=>'event',
    'model'=>'event',
    'directory'=>'events',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'store_route'=>['name'=>'event.category.store'],

])
