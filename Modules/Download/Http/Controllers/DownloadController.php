<?php

namespace Modules\Download\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Category;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Download\Entities\Download;
use Modules\Download\Http\Requests\DownloadRequest;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;

class DownloadController extends Controller
{
    use HasQuestion,HasCategory;

    protected $entity;
    protected $class;


//category

    protected $route_categories_index='download::categories.index';
    protected $route_categories_create='download::categories.create';
    protected $route_categories_edit='download::categories.edit';
    protected $route_categories='download.categories';


//question

    protected $route_questions_index='download::questions.index';
    protected $route_questions_create='download::questions.create';
    protected $route_questions_edit='download::questions.edit';
    protected $route_questions='downloads.index';


//notification

    protected $notification_store='download::downloads.store';
    protected $notification_update='download::downloads.update';
    protected $notification_delete='download::downloads.delete';
    protected $notification_error='download::downloads.error';


    function __construct()
    {
        $this->entity=new Download();

        $this->class=Download::class;

        $this->middleware('permission:role-list')->only(['only'=>['index']]);
        $this->middleware('permission:role-create')->only(['create','store']);
        $this->middleware('permission:role-edit' )->only(['edit','update']);
        $this->middleware('permission:role-delete')->only(['destroy']);
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('download::downloads.index',compact('items'));
        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $categories=Category::latest()->where('model',Download::class)->get();
            return view('download::downloads.create',compact('categories'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(DownloadRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->text=$request->input('text');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();


            $this->entity->analyzer()->create();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if($request->has('download')){
                $this->entity->addMedia($request->file('download'))->toMediaCollection(config('cms.collection-download'));
            }
            if(!$saved){
                return redirect()->back()->with('error',__('download::downloads.error'));
            }else{
                return redirect(route("downloads.index"))->with('message',__('download::downloads.store'));
            }
        }catch (Exception $exception){
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Download::class)->get();
            $item=$this->entity->whereToken($token)->first();
            return view('download::downloads.edit',compact('item','categories'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(DownloadRequest $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "order"=>orderInfo($request->input('order')),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
            ]);
            $this->entity->replicate();

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if($request->has('download')){
                destroyMedia($this->entity,config('cms.collection-download'));
                $this->entity->addMedia($request->file('download'))->toMediaCollection(config('cms.collection-download'));
            }


            if(!$updated){
                return redirect()->back()->with('error',__('download::downloads.error'));
            }else{
                return redirect(route("downloads.index"))->with('message',__('download::downloads.update'));
            }


        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            destroyMedia($this->entity,config('cms.collection-image'));
            destroyMedia($this->entity,config('cms.collection-download'));
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('download::downloads.error'));
            }else{
                return redirect(route("downloads.index"))->with('message',__('download::downloads.delete'));
            }



        }catch (\Exception $exception){
            return abort('500');
        }
    }

}
