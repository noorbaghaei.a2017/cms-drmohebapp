@extends('core::layout.panel')
@section('pageTitle', __('cms.edit'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                            @if(!$item->Hasmedia('images'))
                                    <img style="width: 400px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">


                            @else
                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images')}}" alt="" class="img-responsive">


                            @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.subject')}} : </h6>
                                <h4 style="padding-top: 35px">    {{$item->title}}</h4>
                            </div>
                            <div>
                                <h6 style="padding-top: 35px"> {{__('cms.excerpt')}} : </h6>
                                <p>    {{$item->excerpt}}</p>
                            </div>
                            <div>
                                @if($item->Hasmedia(config('cms.collection-download')))
                                    <h6 style="padding-top: 25px;padding-bottom: 10px;">{{__('cms.download-file')}}د</h6>

                                    <a class="btn btn-primary btn-block" target="_blank" href="{{$item->getFirstMediaUrl(config('cms.collection-download'))}}" >{{$item->title}}</a>


                                @endif

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                        <div class="pull-left">
                            <small>
                                {{__('article::articles.text-edit')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" action="{{route('downloads.update', ['download' => $item->token])}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$item->token}}" name="token">
                            {{method_field('PATCH')}}
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="title" class="form-control-label">{{__('cms.title')}}</label>
                                <input type="text" name="title" class="form-control" id="title"  value="{{$item->title}}">
                            </div>
                            <div class="col-sm-6">
                                <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                @include('core::layout.load-single-image')
                            </div>
                            <div class="col-sm-3">
                                <label for="file" class="form-control-label">{{__('cms.file')}} </label>
                                <input type="file" name="download"   class="form-control" id="file" >
                            </div>

                        </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="order" class="form-control-label">{{__('cms.order')}} </label>
                                    <input type="number" name="order" value="{{$item->order}}" class="form-control" id="order">
                                </div>
                            </div>
                            <div class="form-group row">
                                <span class="text-danger">*</span>
                                <label for="text" class="form-control-label">{{__('cms.text')}}</label>
                                <div class="box m-b-md">
                                    <div class="box m-b-md">
                                        @include('core::layout.text-editor',['item'=>$item->text])
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" value="{{$item->excerpt}}" name="excerpt" class="form-control" id="excerpt" autocomplete="off">
                                </div>


                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="category" class="form-control-label">{{__('cms.category')}}  </label>
                                    <select dir="rtl" class="form-control" id="category" name="category" required>
                                        @foreach($categories as $category)
                                            <option value="{{$category->token}}" {{$category->id==$item->category ? "selected" : ""}}>{{$category->title}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>


                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <input type="submit"  class="btn btn-success btn-sm text-sm" value="{{__('cms.update')}} ">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    order: {
                        number: true,
                        required:true
                    },
                    text: {
                        required: true
                    },
                    excerpt: {
                        required: true
                    },
                    category: {
                        required: true
                    },


                },
                messages: {
                    title:"عنوان الزامی است",
                    text: "متن  لزامی است",
                    order: " فرمت  نادرست",
                    excerpt: "خلاصه  الزامی است",
                    category: "دسته بندی  الزامی است",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
