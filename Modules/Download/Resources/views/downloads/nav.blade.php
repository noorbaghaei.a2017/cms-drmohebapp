<li>
        <a href="{{route('downloads.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('download.icons.download')}}"></i>
                              </span>
            <span class="nav-text">{{__('download::downloads.collect')}}</span>
        </a>
    </li>

