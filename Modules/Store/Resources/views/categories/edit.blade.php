@include('core::layout.modules.category.edit',[

    'title'=>__('core::categories.create'),
    'item'=>$item,
    'parent'=>'store',
    'model'=>'store',
    'directory'=>'stores',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'update_route'=>['name'=>'store.category.update','param'=>'category'],

])








