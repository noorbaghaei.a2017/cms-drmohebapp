<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeksTale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weeks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('saturday',10)->nullable();
            $table->string('sunday',10)->nullable();
            $table->string('monday',10)->nullable();
            $table->string('tuesday',10)->nullable();
            $table->string('wednesday',10)->nullable();
            $table->string('thursday',10)->nullable();
            $table->string('friday',10)->nullable();
            $table->morphs('weekable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weeks');
    }
}
