<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Article\Entities\Article;
use Modules\Brand\Entities\Brand;
use Modules\Carousel\Entities\Carousel;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\Currency;
use Modules\Core\Entities\User;
use Modules\Core\Http\Requests\RoleRequest;
use Modules\Customer\Entities\Customer;
use Modules\Download\Entities\Download;
use Modules\Event\Entities\Event;
use Modules\Information\Entities\Information;
use Modules\Member\Entities\Member;
use Modules\Menu\Entities\Menu;
use Modules\Page\Entities\Page;
use Modules\Plan\Entities\Plan;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Product;
use Modules\Question\Entities\Question;
use Modules\Service\Entities\Advantage;
use Modules\Service\Entities\Property;
use Modules\Service\Entities\Service;
use Modules\Sms\Entities\Sms;
use Modules\Store\Entities\Store;
use Nwidart\Modules\Facades\Module;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $entity;


    function __construct()
    {
        $this->entity=new Role();
        $this->middleware('permission:role-list')->only(['only'=>['index']]);
        $this->middleware('permission:role-create')->only(['create','store']);
        $this->middleware('permission:role-edit' )->only(['edit','update']);
        $this->middleware('permission:role-delete')->only(['destroy']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $items = $this->entity->orderBy('id','DESC')->paginate(config('cms.paginate'));
            return view('core::layout.roles.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        try {
            if(hasModule('Article')){
                $permission_articles = Permission::whereModel(Article::class)->get();
            }
            if(hasModule('Information')){
                $permission_informations = Permission::whereModel(Information::class)->get();
            }
            if(hasModule('Menu')){
                $permission_menus = Permission::whereModel(Menu::class)->get();
            }
            if(hasModule('Customer')){
                $permission_customers = Permission::whereModel(Customer::class)->get();
            }
            if(hasModule('Client')){
                $permission_clients = Permission::whereModel(Client::class)->get();
            }
            if(hasModule('User')){
                $permission_users = Permission::whereModel(User::class)->get();
            }
            if(hasModule('Member')){
                $permission_members = Permission::whereModel(Member::class)->get();
            }
            if(hasModule('Carousel')){
                $permission_carousels = Permission::whereModel(Carousel::class)->get();
            }
            if(hasModule('Page')){
                $permission_pages = Permission::whereModel(Page::class)->get();
            }
            if(hasModule('Service')){
                $permission_services = Permission::whereModel(Service::class)->get();
                $permission_advantages = Permission::whereModel(Advantage::class)->get();
                $permission_properties = Permission::whereModel(Property::class)->get();
            }
            if(hasModule('Question')){
                $permission_questions = Permission::whereModel(Question::class)->get();
            }
            if(hasModule('Product')){
                $permission_products = Permission::whereModel(Product::class)->get();
            }
            if(hasModule('Download')){
                $permission_downloads = Permission::whereModel(Download::class)->get();
            }
            if(hasModule('Event')){
                $permission_events = Permission::whereModel(Event::class)->get();
            }
            if(hasModule('Portfolio')){
                $permission_portfolios = Permission::whereModel(Portfolio::class)->get();
            }
            if(hasModule('Store')){
                $permission_stores = Permission::whereModel(Store::class)->get();
            }
            if(hasModule('Brand')){
                $permission_brands = Permission::whereModel(Brand::class)->get();
            }
            if(hasModule('Plan')){
                $permission_plans = Permission::whereModel(Plan::class)->get();
            }
            if(hasModule('Sms')){
                $permission_smses = Permission::whereModel(Sms::class)->get();
            }




            return view('core::layout.roles.create',compact(
                'permission_articles',
                'permission_menus',
                'permission_informations',
                'permission_carousels',
                'permission_services',
                'permission_advantages',
                'permission_properties',
                'permission_questions',
                'permission_products',
                'permission_events',
                'permission_downloads',
                'permission_portfolios',
                'permission_clients',
                'permission_members',
                'permission_users',
                'permission_customers',
                'permission_pages',
                'permission_plans',
                'permission_stores',
                'permission_brands',
                'permission_smses'
            ));

        }catch (\Exception $exception){
            return abort('500');
        }


    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {


        try {
            $this->entity->name=$request->input('name');
            $this->entity->token=tokenGenerate();

            $this->entity->save();



            $this->entity->syncPermissions($request->input('permission'));


            return redirect()->route('roles.index')
                ->with('success','Role created successfully');
        }catch (\Exception $exception){
            return abort('500');
        }

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $role = Role::find($id);
            $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
                ->where("role_has_permissions.role_id",$id)
                ->get();


            return view('roles.show',compact('role','rolePermissions'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($token)
    {
        try {

            if(hasModule('Article')){
                $permission_articles = Permission::whereModel(Article::class)->get();
            }
            if(hasModule('Information')){
                $permission_informations = Permission::whereModel(Information::class)->get();
            }
            if(hasModule('Menu')){
                $permission_menus = Permission::whereModel(Menu::class)->get();
            }
            if(hasModule('Customer')){
                $permission_customers = Permission::whereModel(Customer::class)->get();
            }
            if(hasModule('User')){
                $permission_users = Permission::whereModel(User::class)->get();
            }
            if(hasModule('Client')){
                $permission_clients = Permission::whereModel(Client::class)->get();
            }
            if(hasModule('Member')){
                $permission_members = Permission::whereModel(Member::class)->get();
            }
            if(hasModule('Carousel')){
                $permission_carousels = Permission::whereModel(Carousel::class)->get();
            }
            if(hasModule('Page')){
                $permission_pages = Permission::whereModel(Page::class)->get();
            }
            if(hasModule('Service')){
                $permission_services = Permission::whereModel(Service::class)->get();
                $permission_advantages = Permission::whereModel(Advantage::class)->get();
                $permission_properties = Permission::whereModel(Property::class)->get();
            }
            if(hasModule('Question')){
                $permission_questions = Permission::whereModel(Question::class)->get();
            }
            if(hasModule('Product')){
                $permission_products = Permission::whereModel(Product::class)->get();
            }
            if(hasModule('Download')){
                $permission_downloads = Permission::whereModel(Download::class)->get();
            }
            if(hasModule('Event')){
                $permission_events = Permission::whereModel(Event::class)->get();
            }
            if(hasModule('Portfolio')){
                $permission_portfolios = Permission::whereModel(Portfolio::class)->get();
            }
            if(hasModule('Store')){
                $permission_stores = Permission::whereModel(Store::class)->get();
            }
            if(hasModule('Brand')){
                $permission_brands = Permission::whereModel(Brand::class)->get();
            }
            if(hasModule('Plan')){
                $permission_plans = Permission::whereModel(Plan::class)->get();
            }
            if(hasModule('Sms')){
                $permission_smses = Permission::whereModel(Sms::class)->get();
            }

            $item = $this->entity->whereToken($token)->firstOrFail();
            $permissions = Permission::get();
            $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$item->id)
                ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
                ->all();


            return view('core::layout.roles.edit',compact(
                'rolePermissions',
                'permissions',
                'item',
                'permission_articles',
                'permission_menus',
                'permission_informations',
                'permission_carousels',
                'permission_services',
                'permission_advantages',
                'permission_properties',
                'permission_questions',
                'permission_products',
                'permission_events',
                'permission_downloads',
                'permission_portfolios',
                'permission_users',
                'permission_clients',
                'permission_members',
                'permission_customers',
                'permission_pages',
                'permission_plans',
                'permission_stores',
                'permission_brands',
                'permission_smses'
            ));
        }catch (\Exception $exception){
            return abort('500');
        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $token)
    {
        try {
            $role = $this->entity->whereToken($token)->firstOrFail();
            $role->name = $request->input('name');
            $role->save();


            $role->syncPermissions($request->input('permission'));


            return redirect()->route('roles.index')
                ->with('success','Role updated successfully');
        }catch (\Exception $exception){
            return abort('500');
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::table("roles")->where('id',$id)->delete();
            return redirect()->route('roles.index')
                ->with('success','Role deleted successfully');
        }catch (\Exception $exception){
            return abort('500');
        }

    }
}
