<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Currency;
use Modules\Core\Http\Requests\CurrencyRequest;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $entity;


    function __construct()
    {
        $this->entity=new Currency();

        $this->middleware('permission:currency-list')->only(['only'=>['index']]);
        $this->middleware('permission:currency-create')->only(['create','store']);
        $this->middleware('permission:currency-edit' )->only(['edit','update']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=Currency::latest()->get();
            return view('core::currencies.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title) &&
                !isset($request->symbol)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('core::currencies.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->where("symbol",'LIKE','%'.trim($request->symbol).'%')
                ->paginate(config('cms.paginate'));
            return view('core::currencies.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('core::currencies.create');
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CurrencyRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->symbol=$request->input('symbol');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();
            if(!$saved){
                return redirect()->back()->with('error',__('core::currencies.error'));
            }else{
                return redirect(route("currencies.index"))->with('message',__('core::currencies.store'));
            }
        }catch (Exception $exception){
            return abort('500');
        }
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('core::currencies.edit',compact('item'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(CurrencyRequest $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                "title"=>$request->input('title'),
                "symbol"=>$request->input('symbol'),
            ]);

            if(!$updated){
                return redirect()->back()->with('error',__('core::currencies.error'));
            }else{
                return redirect(route("currencies.index"))->with('message',__('core::currencies.update'));
            }
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('core::currencies.error'));
            }else{
                return redirect(route("currencies.index"))->with('message',__('core::currencies.delete'));
            }



        }catch (\Exception $exception){
            return abort('500');
        }
    }
}
