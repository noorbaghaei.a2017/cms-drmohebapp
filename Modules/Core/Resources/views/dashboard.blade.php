@extends('core::layout.panel')
@section('pageTitle', __('cms.dashboard'))
@section('content')
    <div class="row-col">
        <div class="col-lg b-r">
           @include('core::layout.access-information')
            @include('core::layout.log-information')

        </div>
    </div>
@endsection
