{{--@if(!$item->Hasmedia('gallery'))--}}

@foreach($medias as $media)
    <div class="col-xs-6 col-sm-3 col-md-2">
        <div class="box p-a-xs">
            <a href="#"><img src="{{$media->getUrl()}}" alt="" class="img-responsive"></a>
            <div class="p-a-sm">
                <div class="text-ellipsis">
                    <a href="{{route($model.'.gallery.destroy',['media'=>$media])}}" class="btn btn-danger btn-sm text-sm text-white">
                        <span class="{{config('cms.icon.delete')}}"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endforeach

{{--@endif--}}
