<?php

namespace Modules\Member\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\Member\Entities\Member;
use Modules\Member\Entities\MemberRole;
use Modules\Member\Http\Requests\MemberRequest;

class SideController extends Controller
{

    protected $entity;

    public function __construct()
    {
        $this->entity=new MemberRole();

        $this->middleware('permission:side-list')->only('index');
        $this->middleware('permission:side-create')->only(['create','store']);
        $this->middleware('permission:side-edit' )->only(['edit','update']);

    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('member::sides.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
             $items=$this->entity
                 ->where("title",trim($request->title))
                 ->paginate(config('cms.paginate'));
            return view('member::sides.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $sides=$this->entity->latest()->get();
            return view('member::sides.create',compact('sides'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param MemberRequest $request
     * @return Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function store(MemberRequest $request)
    {
        try {


            $this->entity->title=$request->input('title');
            $this->entity->token=tokenGenerate();
            $saved=$this->entity->save();

            if(!$saved){
                return redirect()->back()->with('error',__('member::members.error'));
            }else{
                return redirect(route("members.index"))->with('message',__('member::members.store'));
            }

        }catch (\Exception $exception){

            return abort('500');

        }
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            $roles=MemberRole::latest()->get();
            return view('member::members.edit',compact('item','roles'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function update(MemberRequest $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $update=$this->entity->update([
                "title"=>$request->input('firstname'),
            ]);


            if(!$update){
                return redirect()->back()->with('error',__('member::members.error'));
            }else{
                return redirect(route("members.index"))->with('message',__('member::members.update'));
            }

        }catch (\Exception $exception){
            return abort('500');
        }
    }


}
