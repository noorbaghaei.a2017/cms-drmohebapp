@include('core::layout.modules.index',[

    'title'=>__('comment::comments.index'),
    'items'=>$items,
    'parent'=>'comment',
    'model'=>'comment',
    'directory'=>'comments',
    'collect'=>__('comment::comments.collect'),
    'singular'=>__('comment::comments.singular'),
    'create_route'=>['name'=>'comments.create'],
    'edit_route'=>['name'=>'comments.edit','name_param'=>'comment'],
    'destroy_route'=>['name'=>'comments.destroy','name_param'=>'comment'],
     'search_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
    __('cms.update_date')=>'AgoTimeUpdate',
     __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
     __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at'
    ],


])
