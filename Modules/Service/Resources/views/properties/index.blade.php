@include('core::layout.modules.index',[

    'title'=>__('service::properties.index'),
    'items'=>$items,
    'parent'=>'service',
    'model'=>'property',
    'directory'=>'properties',
    'collect'=>__('service::properties.collect'),
    'singular'=>__('service::properties.singular'),
    'create_route'=>['name'=>'properties.create'],
    'edit_route'=>['name'=>'properties.edit','name_param'=>'property'],
    'destroy_route'=>['name'=>'properties.destroy','name_param'=>'property'],
     'search_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
     __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
