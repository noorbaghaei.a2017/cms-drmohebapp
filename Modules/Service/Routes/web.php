<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/services', 'ServiceController')->only('create','store','destroy','update','index','edit');
    Route::resource('/properties', 'PropertyController')->only('create','store','destroy','update','index','edit');
    Route::resource('/advantages', 'AdvantageController')->only('create','store','destroy','update','index','edit');

    Route::group(["prefix"=>'search', "middleware" => ["auth:web"]], function () {
        Route::post('/services', 'ServiceController@search')->name('search.service');
        Route::post('/properties', 'PropertyController@search')->name('search.property');
        Route::post('/advantages', 'AdvantageController@search')->name('search.advantage');
    });

    Route::group(["prefix"=>'services'], function () {
        Route::get('/gallery/{service}', 'ServiceController@gallery')->name('service.gallery');
        Route::post('/gallery/store/{service}', 'ServiceController@galleryStore')->name('service.gallery.store');
        Route::get('/gallery/destroy/{media}', 'ServiceController@galleryDestroy')->name('service.gallery.destroy');
    });

    Route::group(["prefix"=>'service/questions'], function () {
        Route::get('/{service}', 'ServiceController@question')->name('service.questions');
        Route::get('/create/{service}', 'ServiceController@questionCreate')->name('service.question.create');
        Route::post('/store/{service}', 'ServiceController@questionStore')->name('service.question.store');
        Route::delete('/destroy/{question}', 'ServiceController@questionDestroy')->name('service.question.destroy');
        Route::get('/edit/{service}/{question}', 'ServiceController@questionEdit')->name('service.question.edit');
        Route::patch('/update/{question}', 'ServiceController@questionUpdate')->name('service.question.update');
    });

    Route::group(["prefix"=>'service/categories'], function () {
        Route::get('/', 'ServiceController@categories')->name('service.categories');
        Route::get('/create', 'ServiceController@categoryCreate')->name('service.category.create');
        Route::post('/store', 'ServiceController@categoryStore')->name('service.category.store');
        Route::get('/edit/{category}', 'ServiceController@categoryEdit')->name('service.category.edit');
        Route::patch('/update/{category}', 'ServiceController@categoryUpdate')->name('service.category.update');

    });


});
