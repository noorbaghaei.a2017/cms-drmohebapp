<?php

namespace Modules\Service\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Service\Entities\Advantage;
use Modules\Service\Http\Requests\AdvantageRequest;

class AdvantageController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Advantage();

        $this->middleware('permission:advantage-list')->only(['only'=>['index']]);
        $this->middleware('permission:advantage-create')->only(['create','store']);
        $this->middleware('permission:advantage-edit' )->only(['edit','update']);
        $this->middleware('permission:advantage-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $items=$this->entity->latest()->paginate(config('cms.paginate'));
        return view('service::advantages.index',compact('items'));
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('service::advantages.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->paginate(config('cms.paginate'));
            return view('service::advantages.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('service::advantages.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(AdvantageRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->icon=$request->input('icon');
            $this->entity->text=$request->input('text');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();


            if(!$saved){
                return redirect()->back()->with('error',__('service::advantages.error'));
            }else{
                return redirect(route("advantages.index"))->with('message',__('service::advantages.store'));
            }


        }catch (Exception $exception){

        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->firstOrFail();
            return view('service::advantages.edit',compact('item'));
        }catch (\Exception $exception){

        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return Response
     */
    public function update(AdvantageRequest $request, $token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "icon"=>$request->input('icon'),
                "text"=>$request->input('text'),
                "order"=>orderInfo($request->input('order'))
            ]);


            if(!$updated){
                return redirect()->back()->with('error',__('service::advantages.error'));
            }else{
                return redirect(route("advantages.index"))->with('message',__('service::advantages.update'));
            }

        }catch (\Exception $exception){

        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        $this->entity=$this->entity->whereToken($token)->firstOrFail();
        $deleted=$this->entity->delete();

        if(!$deleted){
            return redirect()->back()->with('error',__('service::advantages.error'));
        }else{
            return redirect(route("questions.index"))->with('message',__('service::advantages.delete'));
        }
    }
}
