<?php

namespace Modules\Information\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Info;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Information\Entities\Information;
use Modules\Information\Http\Requests\InformationRequest;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;

class InformationController extends Controller
{
    use HasCategory,HasQuestion;

    protected $entity;
    protected $class;



//category

    protected $route_categories_index='information::categories.index';
    protected $route_categories_create='information::categories.create';
    protected $route_categories_edit='information::categories.edit';
    protected $route_categories='information.categories';


//question

    protected $route_questions_index='information::questions.index';
    protected $route_questions_create='information::questions.create';
    protected $route_questions_edit='information::questions.edit';
    protected $route_questions='informations.index';


//notification

    protected $notification_store='information::informations.store';
    protected $notification_update='information::informations.update';
    protected $notification_delete='information::informations.delete';
    protected $notification_error='information::informations.error';


    public function __construct()
    {
        $this->entity=new Information();
        $this->class=Information::class;

        $this->middleware('permission:information-list')->only('index');
        $this->middleware('permission:information-create')->only(['create','store']);
        $this->middleware('permission:information-edit' )->only(['edit','update']);
        $this->middleware('permission:information-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $categories=Category::latest()->where('model',Information::class)->get();
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('information::informations.index',compact('items','categories'));
        }
        catch (\Exception $exception){
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('information::informations.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->paginate(config('cms.paginate'));
            return view('information::informations.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $categories=Category::latest()->where('model',Information::class)->get();
            return view('information::informations.create',compact('categories'));
        }catch (\Exception $exception){
            return  dd($exception->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param InformationRequest $request
     * @return Response
     */
    public function store(InformationRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->text=$request->input('text');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            $this->entity->analyzer()->create();
            $this->entity->attachTags($request->input('tags'));
            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                return redirect()->back()->with('error',__('information::informations.error'));
            }else{
                return redirect(route("informations.index"))->with('message',__('information::informations.store'));
            }



        }catch (\Exception $exception){
          return dd($exception->getMessage());
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Information::class)->get();
            $item=$this->entity->with('tags')->whereToken($token)->first();
            return view('information::informations.edit',compact('item','categories'));
        }catch (\Exception $exception){
            return  dd($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     * @param InformationRequest $request
     * @param $token
     * @return void
     */
    public function update(InformationRequest $request, $token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
            ]);
            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            $this->entity->syncTags($request->input('tags'));
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$updated){
                return redirect()->back()->with('error',__('information::informations.error'));
            }else{
                return redirect(route("informations.index"))->with('message',__('information::informations.update'));
            }

        }catch (\Exception $exception){

        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            destroyMedia($this->entity,config('cms.collection-image'));
            $this->entity->seo()->delete();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('information::informations.error'));
            }else{
                return redirect(route("informations.index"))->with('message',__('information::informations.delete'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
        }
    }

}
