<?php
return [
    "text-create"=>"you can create your information",
    "text-edit"=>"you can edit your information",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"articles list",
    "singular"=>"information",
    "collect"=>"informations",
    "permission"=>[
        "information-full-access"=>"information full access",
        "information-list"=>"informations list",
        "information-delete"=>"information delete",
        "information-create"=>"information create",
        "information-edit"=>"edit information",
    ]
];
