@include('core::layout.modules.index',[

    'title'=>__('request::requests.index'),
    'items'=>$items,
    'parent'=>'request',
    'model'=>'request',
    'directory'=>'requests',
    'collect'=>__('request::requests.collect'),
    'singular'=>__('request::requests.singular'),
     'search_route'=>true,
    'pagination'=>true,
    'datatable'=>[
    __('cms.email')=>'email',
    __('cms.name')=>'name',
    __('cms.phone')=>'phone',
    __('cms.gender')=>'gender',
    __('cms.loss')=>'loss',
    __('cms.color')=>'color',
    __('cms.fall_time')=>'fall_time',
    __('cms.transplantation')=>'transplantation',
    __('cms.feeling')=>'feeling',
    __('cms.execution_time')=>'execution_time',
      __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
 __('cms.email')=>'email',
    __('cms.name')=>'name',
    __('cms.phone')=>'phone',
    __('cms.gender')=>'gender',
    __('cms.loss')=>'loss',
    __('cms.color')=>'color',
    __('cms.fall_time')=>'fall_time',
    __('cms.transplantation')=>'transplantation',
    __('cms.feeling')=>'feeling',
    __('cms.execution_time')=>'execution_time',
        __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
