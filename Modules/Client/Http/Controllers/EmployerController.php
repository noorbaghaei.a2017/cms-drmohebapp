<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Client\Entities\Client;

class EmployerController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Client();

        $this->middleware('permission:employer-list')->only('index');
        $this->middleware('permission:employer-create')->only(['create','store']);
        $this->middleware('permission:employer-edit' )->only(['edit','update']);
        $this->middleware('permission:employer-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity
                ->latest()
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                })
                ->paginate(config('cms.paginate'));
            return view('client::employers.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('client::employers.create');
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('client::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('client::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
