<?php

return [
    "text-create"=>"you can create your carousel",
    "text-edit"=>"you can edit your carousel",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"carousels list",
    "singular"=>"carousel",
    "collect"=>"carousels",
    "permission"=>[
        "carousel-full-access"=>"carousel full access",
        "carousel-list"=>"carousels list",
        "carousel-delete"=>"carousel delete",
        "carousel-create"=>"carousel create",
        "carousel-edit"=>"edit carousel",
    ]
];
