@extends('core::layout.panel')
@section('pageTitle', __('cms.create'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}} </h2>
                        <small>
                            {{__('portfolio::portfolios.text-create')}}
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
@include('core::layout.alert-danger')
                        <form id="signupForm" role="form" method="post" action="{{route('portfolios.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="title" class="form-control-label">{{__('cms.name')}} </label>
                                    <input type="text" name="title" class="form-control" id="title" required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="customer" class="form-control-label">{{__('cms.customer')}} </label>
                                    <select class="form-control" id="customer" name="customer" required>
                                        @foreach($customers as $customer)
                                            <option value="{{$customer->token}}">{{$customer->title}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                    @include('core::layout.load-single-image')
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="order" class="form-control-label">{{__('cms.order')}} </label>
                                    <input type="number" name="order" value="{{old('order')}}" class="form-control" id="order">
                                </div>

                            </div>
                            <div class="form-group row">
                                <span class="text-danger">*</span>
                                <label for="text" class="form-control-label">{{__('cms.text')}} </label>
                                <div class="box m-b-md">
                                    <div class="box m-b-md">
                                        @include('core::layout.text-editor',['item'=>null])
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" name="excerpt" class="form-control" id="excerpt">
                                </div>
                            </div>

                            @include('core::layout.list-categories',['item'=>null])

                            @include('core::layout.list-tags',['item'=>null])

                            @include('core::layout.modules.seo-box',['seo'=>null])


                            @include('core::layout.create-button')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    customer: {
                        required: true
                    },
                    text: {
                        required: true
                    },
                    order: {
                        number: true,
                        required:true
                    },

                },
                messages: {
                    title:"عنوان الزامی است",
                    customer: "مشتری  لزامی است",
                    text: "متن  لزامی است",
                    order: "فرمت نادرست",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
