<?php
return [
    "text-create"=>"you can create your portfolio",
    "text-edit"=>"you can edit your portfolio",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "error"=>"error",
    "index"=>"portfolios list",
    "singular"=>"portfolio",
    "collect"=>"portfolios",
    "permission"=>[
        "portfolio-full-access"=>"portfolios full access",
        "portfolio-list"=>"portfolios list",
        "portfolio-delete"=>"portfolio delete",
        "portfolio-create"=>"portfolio create",
        "portfolio-edit"=>"edit portfolio",
    ]
];
