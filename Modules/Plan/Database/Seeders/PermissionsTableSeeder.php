<?php

namespace Modules\Plan\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Plan\Entities\Plan;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Plan::class)->delete();

        Permission::create(['name'=>'plan-list','model'=>Plan::class,'created_at'=>now()]);
        Permission::create(['name'=>'plan-create','model'=>Plan::class,'created_at'=>now()]);
        Permission::create(['name'=>'plan-edit','model'=>Plan::class,'created_at'=>now()]);
        Permission::create(['name'=>'plan-delete','model'=>Plan::class,'created_at'=>now()]);
    }
}
