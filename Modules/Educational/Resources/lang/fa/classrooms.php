<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید کلاس جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید کلاس خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست کلاس ها",
    "singular"=>"کلاس",
    "collect"=>"کلاس ها",
    "permission"=>[
        "classroom-full-access"=>"دسترسی کامل به کلاس ها",
        "classroom-list"=>"لیست کلاس ها",
        "classroom-delete"=>"حذف کلاس",
        "classroom-create"=>"ایجاد کلاس",
        "classroom-edit"=>"ویرایش کلاس",
    ]
];
